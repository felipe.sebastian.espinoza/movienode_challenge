# Movie Node Challenge


## Instrucciones

1) Hacer el build de la imagen de docker: 

``` docker-compose build ```
2) Levantar el contenedor(levanta el contenedor de node y el de mongo):

```docker-compose up```

 

### Listo!! el contenedor de node usa el puerto 5000 y el de mongo 27021
Por defecto la base de datos esta vacia

### Comandos


### endpoint1  ejemplo

#### Añadir registro en la base de datos
 (get)localhost:5000/movie/?title=star wars
 
(get)localhost:5000/movie/?title=star wars&year=1980  (año opcional)

 (get)localhost:5000/movie/?title=star wars&year=1983  (mismo nombre diferentes peliculas)

#### Reemplazar el plot 
(post)localhost:5000/movie 
  (formato json o object en swagger)

 {
     "movie": "Star Wars: Episode IV - A New Hope", 
     "find": "Jedi", 
     "replace": "CLM Dev"
 }

### endpoint2 ejemplo
#### Obtiene los registros de la base de datos
 (get)localhost:5000/all/?page=1

## Documentacion en swagger
#### Enlace

https://app.swaggerhub.com/apis/FelipeSebastianEspinoza/MovieNode/1.0.0#/



#### Opciones que usé en swagger hub para hacer peticiones a la api desde ahi al contenedor

server localhost:5000

SSL certificates are not validated 

Routing requests via browser  

Credentials in CORS requests are disabled  
