const Joi = require('joi');

const schema = Joi.object({
    title: Joi.string().required(),
    year: Joi.number(),
});

module.exports = schema