const Joi = require('joi');

const schema = Joi.object({
    page: Joi.number().min(1).required().error((errors) => new Error('la página requiere un número positivo distinto de cero'))
});

module.exports = schema