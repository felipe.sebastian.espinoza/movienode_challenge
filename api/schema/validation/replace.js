const Joi = require('joi');

const schema = Joi.object({
    movie: Joi.string().required(),
    find: Joi.string().required(),
    replace: Joi.string().required(),
});

module.exports = schema