const Joi = require('joi');

const schema = Joi.object({
    Title: Joi.string().required(),
    Year: Joi.string().required(),
    Released: Joi.string().required(),
    Genre: Joi.string().required(),
    Director: Joi.string().required(),
    Actors: Joi.string().required(),
    Plot: Joi.string().required(),
    Ratings: Joi.array().required(),
});

module.exports = schema