const Movie = require('../models/movies');
const pageNumber = require('../schema/validation/page')
const params = require('../schema/validation/params')
const schemaReplace = require('../schema/validation/replace')
 
const axios = require('axios');

exports.moviesControllerAll = async (ctx, next) => {
    const validation = pageNumber.validate(ctx.request.query);
    if (validation.error) {
        ctx.throw(validation.error.message);
    } else {
        try {
            let page = ctx.request.query.page
            let limit = 5
            let movies = await Movie.find()
                .limit(limit * 1)
                .skip((page - 1) * limit)
            ctx.body = movies
        } catch (error) {
            ctx.throw(400, error.message);
        }
    }
}

exports.moviesControllerPost = async (ctx, next) => {
    const validation = schemaReplace.validate(ctx.request.body);
    if (validation.error) {
        ctx.throw(validation.error.message);
    } else {
        try {
            let originalPlot = await Movie.findByTitle(ctx.request.body.movie)
            if (originalPlot) {
                let plot = JSON.stringify(originalPlot);
                let updatedPlot = plot.replace(new RegExp(ctx.request.body.find, "g"), ctx.request.body.replace);
                ctx.body = updatedPlot
            } else {
                ctx.body = "No se encuentra la película"
            }
        } catch (error) {
            ctx.throw(400, error);
        }
    }
}

exports.moviesControllerGet = async (ctx, next) => {
    const validation = params.validate(ctx.request.query);
    if (validation.error) {
        ctx.throw(validation.error.message);
    } else {
        let searchUrl = ''
        let apikey = '&apikey=115166ec'
        let page = 'http://www.omdbapi.com/'
        try {
            if (ctx.request.query.year) {
                searchUrl = page + '?t=' + ctx.request.query.title.trim() + '&y=' + ctx.request.query.year.trim() + '&' + apikey
            } else {
                searchUrl = page + '?t=' + ctx.request.query.title.trim() + apikey
            }
            let response = await axios.get(searchUrl);
            if (response.data.Response == 'False') {
                ctx.body = "No se encontró la película"
            } else {
                let isNewMovie = await Movie.isDuplicateTitle(response.data)
                if (isNewMovie == false) {
                    let movies = await Movie.create(response.data)
                    ctx.body = movies
                } else {
                    ctx.body = isNewMovie
                }
            }
        } catch (error) {
            ctx.throw(400, error);
        }
    }
}



