const { moviesControllerGet, moviesControllerPost, moviesControllerAll } = require("../controllers/movies.controller.js");
const Router = require("koa-router");
const router = new Router();

router.get("/movie", moviesControllerGet);
router.post("/movie", moviesControllerPost);
router.get("/all", moviesControllerAll);


module.exports = router.routes();