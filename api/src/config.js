const mongoose = require('mongoose');


const db = mongoose.connection

mongoose.connect('mongodb://mongo/mongodatabase', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(db => console.log('DB conected to host: ', db.connection.host))
    .catch(err => console.log(err));

db.on('error', (err) => {
    throw new Error('Database failed to connect');
})
db.on('connected', () =>
    console.log('Connected to mongo'))
db.on('disconnected', () =>
    console.log('Mongo is disconnected'))
db.on('open', () =>
    console.log('Database is open'))

module.exports = db;