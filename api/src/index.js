var Koa = require('koa');
var Router = require('koa-router');
var app = new Koa();
var router = new Router();

const db = require('./config')
  
var bodyParser = require('koa-bodyparser');
app.use(bodyParser());

var log4js = require("log4js");
var logger = log4js.getLogger();

var cors = require('koa-cors');
var options = {
  origin: '*'
};
app.use(cors(options));
 
const movieRoute = require("../routes/movies.route");
app.use(movieRoute);
 

app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;
    ctx.body = err.message;
    ctx.app.emit('error', err, ctx);
  }
});
app.on('error', (err, ctx) => {
  logger.level = "debug";
  logger.debug(err.message);
});

app
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(3000)

console.log('server on port:5000')
