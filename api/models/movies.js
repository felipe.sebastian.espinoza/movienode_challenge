const { Schema, model } = require('mongoose')


const movieSchema = new Schema({
    Title: { type: String, required: true },
    Year: { type: String, required: true },
    Released: { type: String, required: true },
    Genre: { type: String, required: true },
    Director: { type: String, required: true },
    Actors: { type: String, required: true },
    Plot: { type: String, required: true },
    Ratings: { type: Array, required: true },
})
//existen peliculas con el mismo nombre 
movieSchema.statics.isDuplicateTitle = async function (Data) {
    try {
        let movieDataBase = await this.findOne({ Title: Data.Title })
        if (movieDataBase) {
            let searchMovie = await this.findOne({ Title: Data.Title, Released: Data.Released })
            if (!searchMovie) {
                return false
            } else {
                return searchMovie
            }
        } else {
            return false
        }
    } catch (error) {
        throw new Error(error.message)
    }
}
movieSchema.statics.findByTitle = async function (Title) {
    try {
        let searchMovie = await this.findOne({ Title })
        if (searchMovie) {
            return searchMovie.Plot
        } else {
            return null
        }
    } catch (error) {
        throw new Error(error.message)
    }
}
module.exports = model("Movie", movieSchema);